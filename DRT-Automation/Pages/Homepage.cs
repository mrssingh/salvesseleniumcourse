﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace Salves_Automation.PageObjects
{
    public class Homepage
    {
        public IWebDriver driver;

        public Homepage(IWebDriver driver)
        {
            this.driver = driver;
            PageFactory.InitElements(driver, this);
        }

        [FindsBy(How = How.XPath, Using = "//*[@class='nav']//*[contains(text(), 'Dashboard')]")]
        public IWebElement DashboardButton { get; set; }

        [FindsBy(How = How.Id, Using = "//*[@id='drop1'][contains(text(), 'Algemeen')]")]
        public IWebElement AlgemeenDropdown  { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@id='drop1'][contains(text(), 'TimeSheets')]")]
        public IWebElement TimeSheetsDropdown { get; set; }

        [FindsBy(How = How.CssSelector, Using = "//*[@id='drop1'][contains(text(), 'Overige')]")]
        public IWebElement OverigeDropdown { get; set; }
    }
}

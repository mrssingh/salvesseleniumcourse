﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace Salves_Automation.PageObjects
{
    public class LoginPage
    {
        public IWebDriver driver;

        public LoginPage(IWebDriver driver)
        {
            this.driver = driver;
            PageFactory.InitElements(driver, this);
        }

        [FindsBy(How = How.CssSelector, Using = "input.btn-default")]
        public IWebElement InloggenButton { get; set; }

        [FindsBy(How = How.CssSelector, Using = "a.btn.btn-default")]
        public IWebElement WachtwoordVergetenButton { get; set; }

        [FindsBy(How = How.Id, Using = "user_email")]
        public IWebElement EmailAdresInputfield { get; set; }

        [FindsBy(How = How.Id, Using = "user_password")]
        public IWebElement WachtoodInputfiel { get; set; }
    }
}

﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace Salves_Automation
{
    [TestFixture]
    public class TestBase
    {
        public IWebDriver driver;

        [SetUp]
        public void Setup()
        {
            driver = new ChromeDriver();
            driver.Navigate().GoToUrl("https://www.salvespersoneel.nl/users/sign_in");
        }

        [TearDown]
        public void SetCleanUp()
        {
            driver.Quit();
        }
    }
}

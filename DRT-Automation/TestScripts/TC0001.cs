﻿using Salves_Automation.PageObjects;
using NUnit.Framework;
using NUnit.Framework.Internal;

namespace Salves_Automation.TestScripts
{
    [TestFixture]
    public class TC0001 : TestBase
    {

        [Test]
        public void LoginWithWrongId()
        {
            LoginPage loginPage = new LoginPage(driver);

            loginPage.EmailAdresInputfield.SendKeys("123@salves.nl");
            loginPage.WachtoodInputfiel.SendKeys("Wachtwoord123");
            loginPage.InloggenButton.Click();     
        }

        [Test]
        public void CorrectLogin()
        {
            LoginPage loginPage = new LoginPage(driver);

            loginPage.EmailAdresInputfield.SendKeys("JuistEmail@salves.nl");
            loginPage.WachtoodInputfiel.SendKeys("JuistWachtwoord");
            loginPage.InloggenButton.Click();
        }

        [Test]
        public void WachtwoordVergeten()
        {
            LoginPage loginPage = new LoginPage(driver);

            loginPage.EmailAdresInputfield.SendKeys("123@salves.nl");
            loginPage.WachtoodInputfiel.SendKeys("Wachtwoord123");
            loginPage.InloggenButton.Click();
            loginPage.WachtwoordVergetenButton.Click();
        }

    }
}
